import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

nomT = 6.460268526681939e-05

def getTvals():
    Tvals = np.genfromtxt('permTvals.csv',delimiter='\n')
    return Tvals
def calcPval(Tvals,nomTval):
    nLarge = 0
    for Tval in Tvals:
        if Tval >= nomTval:
            nLarge += 1
    pValue = float(nLarge)/float(len(Tvals))
    return pValue

def plotTvals(Tvals):
    sns.distplot(Tvals,kde=False,bins=100)
    plt.axvline(x=nomT,color='r',linestyle='dashed',linewidth=2)
    plt.xlabel('Unscaled Permutation T-values')
    plt.ylabel('Entries')
    plt.show()
    
def main():
    Tvals = getTvals()
    pValue = calcPval(Tvals,nomT)
    print("pValue: {}".format(pValue))
    plotTvals(Tvals)

if __name__ == "__main__":
    main()
