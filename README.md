# Energy test using Numba and CUDA

The energy test is a two-sample hypothesis test currently used at LHCb to observe local CP violation in 3- and 4-body decays.
This implementation is based on a [previous implementation.] (https://gitlab.cern.ch/LHCb-BnoC/Manchester_Energy_Test)

This implementation is entirely python based and depends on numba, cupy , numpy and pandas. Please note that these dependicies can be difficult to configure
for different machines due to the implicit dependecies on CUDA runtime libraries so different versions of these may be used that that used for testing. Therefore it is strongly recommended to use
Conda for dealing with the dependencies rather than pip. 

A minimal working example is provided using this repo. This is done by simply running `python eTest.py test1.csv test2.csv -g` which calculates the test statistic T with the flag `-g`
signifying to use a GPU. Not using this flag uses a parallel CPU implementation by default.


There is a known issue related to differences in the value of T when using the GPU or CPU version. This difference although small, does become more apparent with larger datasets.
