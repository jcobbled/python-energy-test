import cupy as cp
import numpy as np
import numba
import math
from numba import cuda
import cmath
import random
import time
from numba import float32,float64,prange

DefaultSigma = 0.5
#threads per block launched
tpb = 32

#parallel cpu implementation
@numba.jit(nopython=True,parallel=True)
def parallel_same_sample(arr1,sigma):
    mysum = 0.0
    for i in prange(arr1.shape[0]):
        for j in prange(i+1,arr1.shape[0]):
            distance = (arr1[i][0] - arr1[j][0])**2
            distance += (arr1[i][1] - arr1[j][1])**2
            distance += (arr1[i][2] - arr1[j][2])**2
            distance += (arr1[i][3] - arr1[j][3])**2
            distance += (arr1[i][4] - arr1[j][4])**2
            mysum += math.exp(-distance/(2*(sigma**2)))
    return mysum

@numba.jit(nopython=True,parallel=True)
def parallel_diff_sample(arr1,arr2,sigma):
    mysum = 0.0
    for i in prange(arr1.shape[0]):
        for j in prange(arr2.shape[0]):
            distance = (arr1[i][0] - arr2[j][0])**2
            distance += (arr1[i][1] - arr2[j][1])**2
            distance += (arr1[i][2] - arr2[j][2])**2
            distance += (arr1[i][3] - arr2[j][3])**2
            distance += (arr1[i][4] - arr2[j][4])**2
            mysum += np.exp(-distance/(2*(sigma**2)))
    return mysum

#functions to calculate just the T value
@cuda.jit('void(float64[:,:],float64[:],float64)')
def gpuSameSample(A,arrSum,sigma):
    tx = cuda.blockDim.x*cuda.blockIdx.x + cuda.threadIdx.x
    tempSum = 0.0
    count = A.shape[0]
    if tx < count:
        temp = A[tx]
        cuda.syncthreads()
        for i in range(tx+1,count):
            distance = (temp[0] - A[i][0])**2
            distance += (temp[1] - A[i][1])**2
            distance += (temp[2] - A[i][2])**2
            distance += (temp[3] - A[i][3])**2
            distance += (temp[4] - A[i][4])**2
            x = -distance/((2.0*sigma**2))
            tempSum +=  math.exp(x)
        arrSum[tx] = tempSum
        cuda.syncthreads()
    
        
@cuda.jit('void(float64[:,:],float64[:,:],float64[:],float64)')
def gpuDiffSample(A,B,arrSum,sigma):
    tx = cuda.blockDim.x*cuda.blockIdx.x + cuda.threadIdx.x
    tempSum = 0.0
    distance = 0.0
    count = A.shape[0]
    countB = B.shape[0]
    if tx <= count:
        temp = A[tx]
        cuda.syncthreads()
        for i in range(countB):
            distance = (temp[0] - B[i][0])**2
            distance += (temp[1] - B[i][1])**2
            distance += (temp[2] - B[i][2])**2
            distance += (temp[3] - B[i][3])**2
            distance += (temp[4] - B[i][4])**2
            tempSum +=  math.exp(-distance/((2.0*sigma**2)))
        arrSum[tx] = tempSum
        cuda.syncthreads()

#T value functions with weights
@cuda.jit
def gpuSameSample_weighted(A,arrSum,sigma,weights):
    tx = cuda.blockDim.x*cuda.blockIdx.x + cuda.threadIdx.x
    tempSum = 0.0
    count = A.shape[0]
    #get weight for this event
    weight_i = weights[tx]
    if tx < count:
        temp = A[tx]
        for i in range(tx+1,count):
            weight_j = weights[i]
            distance = (temp[0] - A[i][0])**2
            distance += (temp[1] - A[i][1])**2
            distance += (temp[2] - A[i][2])**2
            distance += (temp[3] - A[i][3])**2
            distance += (temp[4] - A[i][4])**2
            tempSum +=  weight_i*weight_j*math.exp(-distance/(2*(sigma**2)))
        arrSum[tx] = tempSum

@cuda.jit
def gpuDiffSample_weighted(A,B,arrSum,sigma,weights1,weights2):
    tx = cuda.blockDim.x*cuda.blockIdx.x + cuda.threadIdx.x
    tempSum = 0.0
    count = A.shape[0]
    countB = B.shape[0]
    weight_i = weights1[tx]
    if tx < count:
        temp = A[tx]
        for i in range(countB):
            weight_j = weights2[i]
            distance = (temp[0] - B[i][0])**2
            distance += (temp[1] - B[i][1])**2
            distance += (temp[2] - B[i][2])**2
            distance += (temp[3] - B[i][3])**2
            distance += (temp[4] - B[i][4])**2
            tempSum +=  weight_i*weight_j*math.exp(-distance/(2*(sigma**2)))
    arrSum[tx] = tempSum


#Ti values
@cuda.jit
def gpuSameSampleTi(A,TiArr,sigma):
    tx = cuda.blockDim.x*cuda.blockIdx.x + cuda.threadIdx.x
    tempSum = 0.0
    count = A.shape[0]
    if tx < count:
        temp = A[tx]
        for i in range(count):
            if(i != tx):
                distance = (temp[0] - A[i][0])**2
                distance += (temp[1] - A[i][1] )**2
                distance += (temp[2] - A[i][2])**2
                distance += (temp[3] - A[i][3])**2
                distance += (temp[4] - A[i][4])**2
                tempSum +=  math.exp(-distance/(2.0*(sigma**2.0)))
    TiArr[tx] = tempSum/float(2.0*count*(count - 1))

@cuda.jit
def gpuDiffSampleTi(A,B,TiArr,sigma):
    tx = cuda.blockDim.x*cuda.blockIdx.x + cuda.threadIdx.x
    tempSum = 0.0
    count = A.shape[0]
    countB = B.shape[0]
    if tx < count:
        temp = A[tx]
        for i in range(countB):
            distance = (temp[0] - B[i][0])**2
            distance += (temp[1] - B[i][1] )**2
            distance += (temp[2] - B[i][2])**2
            distance += (temp[3] - B[i][3])**2
            distance += (temp[4] - B[i][4])**2
            tempSum +=  math.exp(-distance/(2.0*(sigma**2.0)))
    TiArr[tx] -= tempSum/float(2.0*count*countB)
        
#function to calculate Tvalue (whole sample or permutation)
def calcTval(arr1,arr2,sigma,gpu):
    tempSum = []
    tempSum2 = []
    tempSum3 = []
    T11 = 0.0
    T12 = 0.0
    T22 = 0.0
    #stream1 = cuda.stream()                                                                                                                                                                                      
    #stream2 = cuda.stream() 
    blocks = 1
    n1 = len(arr1)
    n2 = len(arr2)
    tempSum = cp.zeros(n1,dtype=np.float64)
    tempSum2 = cp.zeros(n2,dtype=np.float64)
    if(n1 >= n2):
        tempSum3 = cp.zeros(n1,dtype=np.float64)
        blocks = int(math.ceil(n1/tpb))
    else:
        tempSum3 = cp.zeros(n2,dtype=np.float64)
        blocks = int(math.ceil(n2/tpb))
    if gpu:
        gpuSameSample[blocks,tpb](arr1,tempSum,sigma)
        gpuSameSample[blocks,tpb](arr2,tempSum2,sigma)
        gpuDiffSample[blocks,tpb](arr1,arr2,tempSum3,sigma)
        T11 = sum(tempSum)
        T22 = sum(tempSum2)
        T12 = sum(tempSum3)
    else:
        T11 = parallel_same_sample(arr1,sigma)
        T22 = parallel_same_sample(arr2,sigma)
        T12 = parallel_diff_sample(arr1,arr2,sigma)

    T = T11/float(n1*(n1 - 1)) + T22/float(n2*(n2 -1)) - T12/float(n1*n2)
    return T

def calcTval_weighted(arr1,arr2,sigma,weights1,weights2):
    tempSum = []
    tempSum2 = []
    tempSum3 = []
    n1 = len(arr1)
    n2 = len(arr2)
    #sum of weights
    w1 = sum(weights1)
    w2 = sum(weights2)
    if(n1 >= n2):
        tempSum = cp.zeros(n1)
        tempSum2 = cp.zeros(n1)
        tempSum3 = cp.zeros(n1)
        blocks = int(math.ceil(n1/tpb))
    else:
        tempSum = cp.zeros(n1)
        tempSum2 = cp.zeros(n1)
        tempSum3 = cp.zeros(n1)
        blocks = int(math.ceil(n2/tpb))
    gpuSameSample_weighted[blocks,tpb](arr1,tempSum,sigma,weights1)
    gpuSameSample_weighted[blocks,tpb](arr2,tempSum2,sigma,weights2)
    gpuDiffSample_weighted[blocks,tpb](arr1,arr2,tempSum3,sigma,weights1,weights2)
    #cuda.synchronize()                                                                                                                                                                                                                                                                   
    T11 = sum(tempSum)
    T22 = sum(tempSum2)
    T12 = sum(tempSum3)

    T = T11/float(w1*w1) + T22/float(w2*w2) - T12/float(w1*w2)
    return T


def calculatePermutations(arr1,arr2,size1,size2,nTvals,sigma,gpu=False):
    Tvals = cp.zeros(nTvals)
    arr1_gpu = cp.asarray(arr1)
    arr2_gpu = cp.asarray(arr2)
    totalArr = cp.concatenate((arr1_gpu,arr2_gpu))
    nCalculated = 0
    n1 = len(arr1)
    n2 = len(arr2)

    while(nCalculated < nTvals):
    #shuffle the array                                                                                                                                                                                            
        cp.random.shuffle(totalArr)
        for j in range(int(len(totalArr)/(size1 + size2))):
            sample1 = totalArr[j*(size1+size2):j*(size1+size2) + size1]
            sample2 = totalArr[j*(size1+size2) + size1:j*(size1+size2) + size1 + size2]
            if(nCalculated %100 == 0):
                print("Calculating T value:{}".format(nCalculated))
            if(nCalculated < nTvals):
                T = calcTval(sample1,sample2,sigma,gpu)
                print(T)
                Tvals[nCalculated] = T
                nCalculated += 1
            elif(nCalculated >= nTvals):
                break

    #unscale the Tvalues
    Tvals = float((size1 + size2)/len(totalArr))*Tvals
    return Tvals

def calcTi(arr1,arr2,sigma):
    n1 = len(arr1)
    n2 = len(arr2)
    Ti = cp.zeros(n1)
    blocks = int(math.ceil(n1/tpb))
    gpuSameSampleTi[blocks,tpb](arr1,Ti,sigma)
    gpuDiffSampleTi[blocks,tpb](arr1,arr2,Ti,sigma)
    return Ti

#calculate Ti permutations
#calculate unique permutations only (unless insufficient events to calculate unique permutations)
def calculateTiPermutations(arr1,arr2,nPerms,sigma):
    n1 = len(arr1)
    n2 = len(arr2)
    arr1_gpu = cp.asarray(arr1)
    arr2_gpu = cp.asarray(arr2)
    temp = cp.concatenate((arr1_gpu,arr2_gpu))
    #array of (min,max) tuples
    TiExtremas_max = cp.zeros(int(nPerms)) 
    TiExtremas_min = cp.zeros(int(nPerms)) 
    for i in range(int(nPerms)):
        #shuffle the array
        cp.random.shuffle(temp)
        temp1,temp2 = cp.split(temp,2)
        blocks = 0
        Ti = cp.zeros(len(temp1))
        if(len(temp1) >= len(temp2)):
            blocks = int(math.ceil(len(temp1)/tpb))
        else:
            blocks = int(math.ceil(len(temp2)/tpb))
            
        Ti = calcTi(temp1,temp2,sigma)
        maxTi = cp.amax(Ti)
        minTi = cp.amin(Ti)
        print(maxTi)
        print(minTi)
        TiExtremas_max[i] = maxTi
        TiExtremas_min[i] = minTi
        #TiExtremas.append((cp.amax(Ti),cp.amin(Ti)))
        #TiExtremas[0] = cp.asarray([cp.amax(Ti),cp.amin(Ti)])
    TiExtremas_max_cpu = cp.asnumpy(TiExtremas_max) 
    TiExtremas_min_cpu = cp.asnumpy(TiExtremas_min) 
    return (TiExtremas_max_cpu,TiExtremas_min_cpu)


#write user interface 
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description = 'Compute the energy test on two different samples')
    parser.add_argument('file1', help='File with first sample')
    parser.add_argument('file2', help='File with second sample')
    parser.add_argument('-t',help='calculate T value and Ti values',action='store_true')
    parser.add_argument('-s','--sigma',help='specify the sigma parameter of the Gaussian metric', default=0.5, type=float)
    parser.add_argument('-n','--nevts',help='limit number of events in each sample to number given', type=float)
    parser.add_argument('-p','--nperm',help='define number of permutations to run', default=0, type=int)
    parser.add_argument('-r','--seed',help='specify a seed for the random number generator', default=0, type=int)
    parser.add_argument('-d',help='skip the default T calculation (used when just adding permutations as a separate job)',action='store_true')
    parser.add_argument('-o','--outfile',help='output file name, do not specify the extension as it will create a Tis.txt and a Ts.txt', default='')
    parser.add_argument('-w','--weights',help='calculate T value given weights',default=False,type=bool)
    parser.add_argument('-g','--gpu',help='Use GPU for calculations. Default: Parallel CPU',action='store_true')
    args = parser.parse_args()

    print("sigma is {}".format(args.sigma))
    print("seed used is {}".format(args.seed))
    cp.random.seed(args.seed)
    
    sample1 = None
    sample2 = None
    weights1 = None
    weights2 = None
    print("Reading Data files")
    if(args.weights == True):
        data1 = np.loadtxt(args.file1,dtype=np.float64)
        data1 = np.loadtxt(args.file2,dtype=npfloat64)
        sample1,weights1 = [list(arr) for arr in zip(*data1)]
        sample2,weights2 = [list(arr) for arr in zip(*data2)]
    else:
        sample1 = np.loadtxt(args.file1)
        sample2 = np.loadtxt(args.file2)
    print("Read in {} events for sample 1".format(len(sample1)))
    print("Read in {} events for sample 2".format(len(sample2)))
    T0 = None
    Tis = None
    TiBars = None
    gpu = args.gpu
    if not args.d:
        if not args.t:
            if (args.weights):
                #setting weights to one for debugging purposes                                                                      
                weights1 = np.random.uniform(0,1,len(sample1)).astype(np.float64)
                weights2 = np.random.uniform(0,1,len(sample2)).astype(np.float64)
                print(weights1[0:10])
                print(weights2[0:10])
                print("Calculating Weighted T value")
                T0 = calcTval_weighted(sample1,sample2,args.sigma,weights1,weights2)
            else:
                print("Calculating unweighted T value")
                if gpu == True:
                    print("Calculating on GPU")
                else:
                    print("Calculating using parallel CPU")
                T0 = calcTval(sample1,sample2,args.sigma,gpu)
                print("Calculated T0!")
        elif args.t:
            print("Calculating Ti values")
            Tis = calcTi(sample1,sample2,args.sigma)
            TiBars = calcTi(sample2,sample1,args.sigma)
            print(len(Tis))
            print(len(TiBars))
            np.savetxt("Ti-values.csv",cp.asnumpy(Tis))
            np.savetxt("Tibar-values.csv",cp.asnumpy(TiBars))
            T0 = sum(Tis) + sum(TiBars)
        print("Nominal T value: {} ".format(T0))
        #TO DO add file output

    if args.nperm != 0 and args.nevts:
        totalEvents = len(sample1) + len(sample2)        
        size1 = args.nevts
        if(len(sample1) == len(sample2)):
            size2 = size1
        #set the relative event size i.e if sample 2 is 2% larger than sample 1
        # the sample size from sample 2 should also be 2% larger than the sample size from sample1
        elif(len(sample1) < len(sample2)):
            size2 = int((len(sample2)/len(sample1))*size1)
        elif(len(sample1) > len(sample2)):
            size2 = int(round((len(sample1)/len(sample2))*size1))
        if(size1 > totalEvents or size2 > totalEvents or (size1 + size2) > totalEvents):
            print("Sample sizes are too large! Can't run permutations. Choose a sensible permutation size!")
        else:
            print("Running permutations with sizes of {0} for sample 1 and {1} for sample 2".format(size1,size2))
            permTvals = calculatePermutations(sample1,sample2,size1,size2,args.nperm,args.sigma,gpu)
            permTvals_host = cp.asnumpy(permTvals)
            print("Unscaling T values")
            permTvals_host = permTvals_host*(size1 + size2)/float(len(sample1) + len(sample2))
            np.savetxt('testTvals.csv',permTvals_host,delimiter=' ')
            #calculate p-value
            largeTvals = 0
            for val in permTvals_host:
                if val >= T0:
                    largeTvals += 1
            p_value = float(largeTvals)/len(permTvals_host)
            print("p-value: {} ".format(p_value))
        #TO DO add file output
    if args.t and args.nperm != 0:
        print("calculating Ti permutations")
        TiExtremas = calculateTiPermutations(sample1,sample2,args.nperm,args.sigma)
        print(TiExtremas)
        #TO DO add file output
        


